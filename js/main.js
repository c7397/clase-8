const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
camera.position.z = 5;

const light = new THREE.PointLight( 0xFFFFFF, 1, 100 );
light.position.set( 0, 0, 5 );
scene.add( light );

const renderer = new THREE.WebGLRenderer();

renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

let bodyHeight = $( "body" ).height();
let scrollPercentage;
$( window ).on( 'scroll', function(){
  scrollPercentage = ( $( window ).scrollTop() ) / (bodyHeight - window.innerHeight) * 100;
  scrollEvent();
});

let icoContainer = new THREE.Object3D();
scene.add( icoContainer );

let count = 300;
let minRad = 0.01;
let maxRad = 0.5;
let maxZ = 100;

for( let i = 0; i < count; i++ ){
  let radius = minRad + ( Math.random() * maxRad );
  let geometry = new THREE.IcosahedronGeometry( radius, 0 );
  let material = new THREE.MeshLambertMaterial( { color: 0xFFFFFF } );
  let ico = new THREE.Mesh( geometry, material );
  ico.position.x = -9 + (Math.random() * 18);
  ico.position.y = -9 + (Math.random() * 18);
  ico.position.z = - (Math.random() * 150);

  icoContainer.add( ico );
}

let geometry = new THREE.BoxGeometry( 0.7, 0.7, 0.7 );
let material = new THREE.MeshLambertMaterial({ color: 0x0000FF });
let movingBox = new THREE.Mesh( geometry, material );
movingBox.position.z = -20;
scene.add( movingBox );

let geometry2 = new THREE.BoxGeometry( 1, 1, 1 );
let material2 = new THREE.MeshLambertMaterial({ color: 0x00FF00 });
let crossingBox = new THREE.Mesh( geometry2, material2 );
crossingBox.position.z = -50;
crossingBox.position.x = -10;
scene.add( crossingBox );

let runAnimation1 = false;

function animate(){
  requestAnimationFrame( animate );

  camera.position.z = -scrollPercentage;
  light.position.z = -scrollPercentage;

	renderer.render( scene, camera );

  if( runAnimation1 == true ){
    if( crossingBox.position.x <= 10 ){
      crossingBox.position.x += 0.1;
    }
  }


}
animate();

let firstTrigger = false;
let secondTrigger = false;

function scrollEvent(){
  //animar caja azul
  movingBox.rotation.y = toRad( scrollPercentage ) * 50;
  //console.log( scrollPercentage );
  //aparecer caja roja
  if( scrollPercentage > 10 && firstTrigger == false ){
    //console.log( "44444" );
    let geometry = new THREE.BoxGeometry( 1, 1, 1 );
    let material = new THREE.MeshLambertMaterial({ color: 0xFF0000});
    let box = new THREE.Mesh( geometry, material );
    scene.add( box );
    box.position.z = camera.position.z - 3;
    firstTrigger = true;
  }

  if( scrollPercentage > 36.5 && secondTrigger == false ){
    runAnimation1 = true;
    secondTrigger = true;
  }

  if( scrollPercentage > 50 ){
    crossingBox.position.x = -10;
  }


}

function toRad( deg ){
  return deg * ( Math.PI/180 );
}
